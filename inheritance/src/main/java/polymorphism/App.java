package polymorphism;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        // System.out.println("Hello World!");

        // Book book = new Book("Danny's book", "Danny the author");
        // System.out.println(book);

        // Ebook eBook = new Ebook("Danny's book", "Danny the author", 500);

        // System.out.println(eBook);

        Book[] bookstore = new Book[5];
        bookstore[0] = new Book("Danny's book", "Danny the author");
        bookstore[1] = new Ebook("Danny's book", "Danny the author", 500);
        bookstore[2] = new Book("Danny's book", "Danny the author");
        bookstore[3] = new Ebook("Danny's book", "Danny the author", 600);
        bookstore[4] = new Ebook("Danny's book", "Danny the author", 700);

        for (Book b : bookstore) {
            System.out.println(b);
        }
        bookstore[1].getNumberBytes();

    }

}
