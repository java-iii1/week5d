package polymorphism;

public class Ebook extends Book {
    private int numberBytes;

    // private String author;
    public Ebook(String title, String author, int numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public int getNumberBytes() {
        return this.numberBytes;
    }

    public String toString() {
        String fromBase = super.toString() + " Byte(s): " + this.numberBytes;
        return fromBase;
    }
}
