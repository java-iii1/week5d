package polymorphism;

public class Book {
    protected String title;
    private String author;

    public Book(String title, String author) {
        this.author = author;
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public String toString() {
        String string = "Author: " + this.author + " Title: " + this.title;

        return string;
    }

}
